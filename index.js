import express from "express";
import mongoose from "mongoose";
import ProductRoutes from "./src/routes/productRoutes.js";
import ChatRoutes from "./src/routes/chatRoutes.js";
import AuthRoutes from "./src/routes/authRoutes.js"

import { getAllProducts } from "./src/services/productService.js";
import { saveNewMessage } from ".//src/services/chatService.js"

import { Server } from "socket.io";
import http from "http";
import session from "express-session"
import MongoStore from 'connect-mongo';
import passport from "./src/passport/passport.js"

const app = express();
const port = 8080;
const baseUrl = "/api";


const server = http.createServer(app);
const io = new Server(server);
app.locals.io = io;
const mongoDbUrl = "mongodb+srv://testUser:testPassword@cluster0.soy6d.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

try {
  mongoose.connect(mongoDbUrl, { useNewUrlParser: true })
} catch (e) {
  console.log(e)
  throw new Error("mongo db cannot be connected");
}
app.use(session({
  secret: "secret",
  resave: false,
  saveUninitialized: false,
  store: new MongoStore(
    {
      mongoUrl: mongoDbUrl,
      mongoOptions: {
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    }
  )
}))
app.use(passport.initialize());
app.use(passport.session());

io.on("connection", async (socket) => {
  console.log("a user connected");

  const products = await getAllProducts();
  io.emit("initialization", { products });

  socket.on("new message", (msg) => {
    io.emit("messages", msg);
    saveNewMessage(msg);

  });

});

app.use(express.json());
app.use(express.static("public"));

app.use(baseUrl, AuthRoutes)
app.use(baseUrl, ProductRoutes);
app.use(baseUrl, ChatRoutes);

server.listen(port, async () => {
  console.log(`app listening at http://localhost:${port}`);
});


