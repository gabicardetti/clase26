const baseProduct = "/productos";
import express from "express";


import * as ProductController from "../controllers/productController.js"

const routes = express.Router();

routes.get(baseProduct + "/listar", ProductController.getAllProducts);

routes.get(baseProduct + "/listar/:id", ProductController.getProductById);

routes.post(baseProduct + "/guardar/", ProductController.createNewProduct);

routes.put(baseProduct + "/actualizar/:id", ProductController.updateProductById);

routes.delete(baseProduct + "/borrar/:id", ProductController.deleteProductById);

export default routes;