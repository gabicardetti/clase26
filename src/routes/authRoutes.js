const baseUrl = "/auth";
import express from "express";
import passport from "passport"


import * as AuthController from "../controllers/authController.js"

const routes = express.Router();

routes.post(baseUrl + "/login", passport.authenticate('login', { failureRedirect: 'badLogin' }), AuthController.login);
routes.post(baseUrl + "/logout", AuthController.logout);
routes.post(baseUrl + "/register", passport.authenticate('register', { failureRedirect: 'badRegister' }), AuthController.register);
routes.get(baseUrl + "/badRegister", AuthController.badRegister);
routes.get(baseUrl + "/badLogin", AuthController.badLogin);

routes.get("/me", AuthController.me);


export default routes;