import UserRepositoryMongo from "../repositories/userRepositoryMongo.js"
import bcrypt from "bcryptjs"

const repository = new UserRepositoryMongo();

export const getAllProducts = async () => repository.getAll();

export const createNewUser = async (username, password) => {
  const hashedPassword = bcrypt.hashSync(password, 10);
  const savedUser = await repository.save({
    username, hashedPassword
  });
  return savedUser;
};

export const getUserByUsername = async (username) => {
  const user = await repository.getByUsername(username);
  return user;
};
