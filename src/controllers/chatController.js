import { getAllMessagesFromFile } from "../services/chatService.js"

export const getAllMessages = async (req, res) => {
    const messages = await getAllMessagesFromFile();

    res.send({ messages });
}