import mongoose from "mongoose"

const schema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true

    },
    date: {
        type: String,
        default: new Date().toISOString()
    }
});

export default mongoose.model("Message", schema);