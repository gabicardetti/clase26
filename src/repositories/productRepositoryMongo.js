import Product from "../models/Product.js"
import mongoose from "mongoose";

export default class ProductRepositoryMongo {

    constructor() {
    }

    async getAll() {
        return Product.find();
    }

    async save(dto) {
        const product = new Product({
            title: dto.title,
            price: dto.price,
            thumbnail: dto.thumbnail
        });

        return product.save();
    }

    async getById(id) {
        let product
        try {
            const mongoId = mongoose.Types.ObjectId(id);
            product = await Product.findOne({ _id: { $eq: mongoId } });
        } catch (e) {
            return;
        }
        
        return product;
    }

    async deleteById(id) {
        let product;
        try {
            const mongoId = mongoose.Types.ObjectId(id);
            product = await Product.deleteOne({ _id: { $eq: mongoId } });
        } catch (e) {
            return;
        }
        
        return product;
    }

    async updateById(id, object) {
        let product;
        try {
            const mongoId = mongoose.Types.ObjectId(id);
            product = await Product.updateOne({ _id: { $eq: mongoId }}, object);
        } catch (e) {
            return;
        }
        
        return product;
    }

}