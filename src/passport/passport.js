import passport from 'passport';
import passportLocal from 'passport-local';
import { getUserByUsername, createNewUser } from "../services/userService.js"
import bcript from "bcryptjs"

const LocalStrategy = passportLocal.Strategy
//----------------
//
//    RECORDAR QUE POR DEFECTO PASSPORT TOMA USERNAME COMO DATO,
//    SI LO CAMBIAMOS (POR NOMBRE, MAIL U OTRA COSA) HAY QUE AGREGAR
//    EN EL MIDDLEWARE EL DATO usernameField: 'nombre'
//
//-------------------

passport.use('login', new LocalStrategy({
    passReqToCallback: true
},
    async function (req, username, password, done) {
        const user = await getUserByUsername(username);
        console.log(user)
        if (!user)
            return done(null, false)

        if (!bcript.compareSync(password, user.password))
            return done(null, false)

        return done(null, user);
    })
);

passport.use('register', new LocalStrategy({
    passReqToCallback: true
},
    async function (req, username, password, done) {
       const findOrCreateUser = async function () {
            //busco usuario
            const user = await getUserByUsername(username);
            if (user) return done(null, false)

            const newUser = await createNewUser(username, password);
            return done(null, newUser);
        }
        process.nextTick(findOrCreateUser);
    })
)

passport.serializeUser(function (user, done) {
    done(null, user.username);
});

passport.deserializeUser(async function (username, done) {
    const user = await getUserByUsername(username);
    done(null, user);
});

export default passport;