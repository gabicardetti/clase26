
# Clase 26

## Antes de correr el proyecto
Tenemos que tener mongodb instalado y corriendo

## Correr proyecto
```
yarn install
node index.js
```

<br>
La ruta para ver todo seria 

http://localhost:8080/

Dejo el postman para probar todas las rutas

Y arme una ruta extra para consultar el historial del chat 

GET localhost:8080/api/chat



Agrege 3 enpoints mas que son 

api/auth/login Para logearse
api/auth/logout Para deslogearse
api/me Para traer la informacion de la session



## Persistir session en mongo

Si se quiere probar local con solamente tener corriendo mongo db suficiente.

Si se quiere usar mongoAtlas deberiamos ir a el index.js y cambiar al valor de la variable
mongoDbUrl y conectarse a mongoAtlas por ejml

```
const mongoDbUrl=mongodb+srv://<user>:<password>@cluster0.soy6d.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
```

Lo dejo configurado con una db de prueba