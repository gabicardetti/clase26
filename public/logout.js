fetch("api/me").then(function (response) {
    return response.json();
}).then(function (data) {
    console.log(data, "dataa", data == {})
    if(data && Object.keys(data).length === 0){
        window.location = "/login.html"
        return
    }
    if (data.name) {
        document.getElementById("logoutText").innerHTML = "Hasta luego " + data.name;
    }
    const url = "/api/auth/logout";
    const request = new XMLHttpRequest();
    request.open("POST", url, true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.onload = function () {
    };
    request.onerror = function (e) {};
  
    request.send(null);
    setTimeout(() => {
        window.location = "/login.html"
    }, 2000);
}).catch(function (e) {
    console.log(e)
});