const showErrorMessage = () => {
  document.getElementById("productTable").innerHTML = `<div class="errorBox">
            <h1 class="errorText">No se encontraron productos</h1>
        </div>`;
};

const showTable = (products) => {
  const table = generateTable(products);
  document.getElementById("productTable").innerHTML = table;
};

const generateTable = (products) => {
  let html = `
    <table class="table">
        ${generateHeader(["Nombre", "Precio", "Foto"])}
        ${generateTableBody(products)}
    </table>
    `;
  return html;
};

const generateHeader = (items) => {
  let header = "<thead > <tr>";
  items.forEach((item) => {
    header += `<th class="text-center" >${item}</td >`;
  });
  header += "</tr> </thead>";
  return header;
};

const generateTableBody = (products) => {
  let tableBody = `<tbody id="tableBody">`;

  products.forEach((product) => {
    tableBody += generateRow(product);
  });
  tableBody += "</tbody>";
  return tableBody;
};

const generateRow = (product) => {
  let row = "";
  row += "<tr>";
  row += `<td align="center" >${product.title}</td>`;
  row += `<td align="center" >${product.price}</td>`;
  row += `<td align="center" ><img src="${product.thumbnail}" alt="Imagen no valida"></td>`;
  row += "</tr>";
  return row;
};

const appendRowToTable = (product) => {
  const row = generateRow(product);
  const tableBody = document.getElementById("tableBody");
  tableBody.innerHTML += row;
};

const socket = io();

socket.on("initialization", (info) => {
  if (info.products.length == 0) {
    showErrorMessage();
  } else {
    showTable(info.products);
  }
});

socket.on("new product", (product) => {
  const div = document.getElementById("productTable");
  if (div.children[0].className == "table") {
    // tengo que apendear elementos a la lista
    appendRowToTable(product);
  } else {
    // tengo que generar la tabla con el primer elemento
    showTable([product]);
  }
});
