function formSubmit(event) {
  event.preventDefault();

  const username = document.getElementById("fUsername").value;
  const password = document.getElementById("fPassword").value;

  if (!username || username == "") {
    alert("El nombre no tiene que estar vacio");
    return;
  }

  if (!password || password == "") {
    alert("La contraseña no tiene que estar vacio");
    return;
  }

  const body = {
    username,
    password
  };

  const url = "/api/auth/register";
  const request = new XMLHttpRequest();
  request.open("POST", url, true);
  request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  request.onload = function () {
    document.getElementById("fUsername").value = "";
    document.getElementById("fPassword").value = "";
    window.location = 'login.html';
  };

  request.onerror = function (e) { };

  request.send(JSON.stringify(body));
}
document.getElementById("form").addEventListener("submit", formSubmit);
