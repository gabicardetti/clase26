function formSubmit(event) {
  event.preventDefault();

  const username = document.getElementById("fName").value;
  const password = document.getElementById("fPassword").value;

  if (!username || username == "") {
    alert("El nombre no tiene que estar vacio");
    return;
  }
  if (!password || password == "") {
    alert("La password no tiene que estar vacia");
    return;
  }
  const body = {
    username,
    password
  };

  fetch("/api/auth/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=UTF-8"
    },
    body: JSON.stringify(body)
  }).then(function (response) {
    return response.json();
  }).then(function (data) {
    console.log(data)
    window.location = 'index.html';
  }).catch(function (e) {
    console.log(e)
  });
}
document.getElementById("form").addEventListener("submit", formSubmit);


document.querySelector("#registerButton").addEventListener("click", function (e) {
  window.location = 'register.html';
})
